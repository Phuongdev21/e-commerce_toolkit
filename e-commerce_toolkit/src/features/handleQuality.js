import { createSlice } from "@reduxjs/toolkit";

const handleQuality = createSlice({
  name: "handleQuality",
  initialState: { rating: null },
  reducers: {
    setRating: (state, action) => (state.rating = action.payload),
  },
});

export const { setRating } = handleQuality;

export default handleQuality.reducer;

import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import thunk from "redux-thunk";
import Home from "../view/Home";
import service from "../service/config";

export const setProductv2 = createAsyncThunk(
  "setProductv2",
  async (url) => {
    try {
      const result = await fetch(url);
      const listData = await result.json();
      return listData;
    } catch (error) {
      console.log(error)
    }
  }
);

const handleProduct = createSlice({
  name: "handleProduct",
  initialState: { product: [] },
  reducers: {
    setProduct: (state, action) => {
      state.product = action.payload;
    },
  },
  extraReducers: {
    [setProductv2.pending]: (state, action) => {
      state.loading = "pending";
    },
    [setProductv2.fulfilled]: (state, action) => {
      state.loading = "success";
    },
    [setProductv2.rejected]: (state, action) => {
      state.loading = "";
    },
  },
});

export const { setProduct } = handleProduct.actions;

export default handleProduct.reducer;

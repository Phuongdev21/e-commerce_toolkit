import React from "react";
import { Route, Routes } from "react-router-dom";
import router from "../../router";
import DefaultFooter from "./DefaultFooter";
import DefaultHeaeder from "./DefaultHeaeder";

const DefaultLayout = () => {
  return (
    <div>
      <DefaultHeaeder />
      <Routes>
        {router.map((route, key) => (
          <Route
            key={key}
            path={route.path}
            name={route.name}
            element={<route.component />}
          />
        ))}
      </Routes>
      <DefaultFooter />
    </div>
  );
};

export default DefaultLayout;

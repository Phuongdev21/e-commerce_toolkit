import "./App.css";
import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";


//import CSS
import "../src/view/Home/home.css";
import "./css/header.css";
import "./css/aside.css";
import "./css/Search.css";
import "./css/Raitings.css";
import "./css/ProductDeatl.css";

import DefaultLayout from "./container/DefaultLayout/DefaultLayout";
function App() {
  return (
    <div>
      <Router><DefaultLayout /></Router>
    </div>
  );
}

export default App;

import Home from "./view/Home";
import DetailProduct from "./view/DetailProduct";

let router = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/home",
    name: "Home",
    component: Home,
  },
  {
    path: "/detailProduct",
    name: "DetailProduct",
    component: DetailProduct,
  },
];

export default router;

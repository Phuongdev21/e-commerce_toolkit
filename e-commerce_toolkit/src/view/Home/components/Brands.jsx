import React from 'react';
import BrandsItem from "./BrandsItem";
function Brands(props) {
  const onChange = (e) => {
    props.handleOnchange(e);
  }
  return (
    <div className='container'>
      <div className='brands'>
        <h3>Brands</h3>
      </div>
      <div className='brands-search'>
          <BrandsItem onChange={(e) => onChange(e)} setSearch={props.setSearch1} />
          </div>
    </div>
  );
}

export default Brands

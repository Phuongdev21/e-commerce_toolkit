import React, { useState, useEffect } from "react";

const Product = ({ data = [], id }) => {
  const [product, setProduct] = useState([]);
  useEffect(() => {
    if (id) {
      data.forEach((item) => {
        if (item.category === id) {
          setProduct(item);
        }
      });
    }
  }, [id, data]);

  const listImg = data.map((img, index) => {
    return (
      <div key={img.id}>
        <div className="ProcductDetail_img">
          <img
            src={img.image}
            alt="Lỗi"
            style={{ marginTop: "16px", textAlign: " center" }}
          />
        </div>
        <span
          className="ProcductDetail_title"
          style={{ textAlign: " center", marginTop: "12px" }}
        >
          {img.name}
        </span>
        <div className="ProcductDetail_ratings">
          {/* <div><Ratings rate={img.rating} /></div> */}
          <div>{img.price}</div>
        </div>
      </div>
    );
  });

  return <div id="main">{listImg}</div>;
};

export default Product;
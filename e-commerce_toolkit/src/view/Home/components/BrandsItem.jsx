import React, { useState, useEffect, useRef } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSearch } from "@fortawesome/free-solid-svg-icons";

const BrandsItem = (props) => {
  // const [isSearch, setSearch] = useState(false);
  const [input, setInput] = useState("");
  const inputRef = useRef(null);
  const [a, setA] = useState([
    { name: "Insigia", qty: 5, status: false },
    { name: "Samsung", qty: 5, status: false },
    { name: "Metra", qty: 5, status: false },
    { name: "HP", qty: 5, status: false },
    { name: "Apple", qty: 5, status: false },
  ]);

  useEffect(() => {
    inputRef.current.focus();
  }, []);
  const handleChange = (e) => {
    setInput(e.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    props.setSearch(input)
  };

  const handleFilterBrand = (el) => {
    const b = { ...el };
    b.status = !b.status;
    const index = a.findIndex(z => z.name === b.name);
    const m = [...a];
    m[index] = b;
    setA(m)
    props.onChange(b);
  };


  return (
    <div>
      <form  className="brands-search">
        <input
          placeholder="Search a product"
          value={input}
          onChange={handleChange}
          name="text"
          ref={inputRef}
        />
        <button onClick={handleSubmit} className="brands-btn-checkBox">
          <FontAwesomeIcon icon={faSearch} />
        </button>
      </form>
      <div className="brands-checkBox">
        {a.map((el, index) => {
          return (
            <div key={index} onClick={() => handleFilterBrand(el)}>
              {/* <input type="checkbox" onChange={handleCheckbox} /> */}
              {el.name}
              <span className="facet-count" />
              {el.qty}
              <br />
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default BrandsItem;

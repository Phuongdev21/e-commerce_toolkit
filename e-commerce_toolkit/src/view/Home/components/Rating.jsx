import React from "react";
import { FaRegStar, FaStar } from "react-icons/fa";
const Rating = ({ value, onChange }) => {
  console.log(value);
  return (
    <div>
      {rating.map((rate) => (
        <div>
          <input
            type="radio"
            name="rating"
            id={rate}
            value={rate}
            style={{ display: "none" }}
            onChange={(e) => {
              onChange(e.target.value);
            }}
          />
          <label htmlFor={rate}>
            {value >= rate ? <FaStar /> : <FaRegStar />}
          </label>
        </div>
      ))}
      {/* <FontAwesomeIcon icon={faRadiationAlt}/>
      <FontAwesomeIcon icon={faRainbow}/> */}
    </div>
  );
};

const rating = [1, 2, 3, 4, 5];

export default Rating;

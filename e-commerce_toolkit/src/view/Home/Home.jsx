import React, { useState, useEffect } from "react";
import Brands from "./components/Brands";
import DynamicListItem from "./components/DynamicListItem";
import Ratings from "./components/Rating";
import Product from "./components/Product";
import { useParams } from "react-router-dom";
import { host } from "../../service/config";
import { useSelector, useDispatch } from "react-redux";
import { setProduct, setProductv2 } from "../../features/handleProduct";

const Home = () => {
  
  const data = useSelector((state) => state.handleProduct.product);
  // const [data, setdatas] = useState([]);

  // const [searchInput, setSearchInput] = useState('');
  const [filterByBrand, setFilterByBrand] = useState({});
  const { id } = useParams();
  const [rating, setRating] = useState(4);
  const [price, setPrice] = useState("");
  const [min, setMin] = useState("");
  const [max, setMax] = useState(4000);
  const [page, setPage] = useState(1);
  const [search, setSearch] = useState("");
  const [sort, setSort] = useState("");

  const dispatch = useDispatch();

  const handeChangeRating = (value) => {
    setRating(value);
  };
  const onChange = (e) => {
    if (e.status === true) {
      setFilterByBrand(e);
    } else {
      setFilterByBrand({});
    }
  };

  useEffect(() => {
    fetch(host)
      .then((res) => res.json())
      .then((datas) => {
        localStorage.setItem("data", JSON.stringify(datas));
        dispatch(setProduct(datas));
        // setdatas(datas);
      });
  }, []);
  //
  const fetchData = async (query) => {
    const listData = await dispatch(setProductv2(`${host}${query}`))
    dispatch(setProduct(listData.payload));
    // setdatas(listData);
  };
  useEffect(() => {
    if (JSON.stringify(filterByBrand) !== "{}") {
      fetchData(`?brand=${filterByBrand.name}`);
    }
  }, [filterByBrand]);

  useEffect(() => {
    fetchData(`?rating=${rating}`);
  }, [rating]);

  useEffect(() => {
    fetchData(`?${price}`);
  }, [price]);

  useEffect(() => {
    fetchData(`?price_gte=${min}&price_lte=${max}`);
  }, [min, max]);

  useEffect(() => {
    fetchData(`?name_like=${search}`);
  }, [search]);

  useEffect(() => {
    fetchData(`?_sort=${sort}`);
  }, [sort]);

  useEffect(() => {
    fetchData(`?_page=${page}&_limit=5`);
  }, [page]);

  //  Math.floor(data.length/8)
  const [menu, setMenu] = useState([
    {
      name: " Appliances",
      subMenu: {
        active: false,
        sub: [
          {
            name: " Dishwashers",
            subMenu: {
              sub: [
                {
                  name: " Built-In Dishwashers",
                  value: "",
                },
              ],
            },
          },
          {
            name: "Fans",
            subMenu: {
              sub: [
                {
                  name: "Window Fans",
                },
              ],
            },
          },
          {
            name: " Cell Phone ",
            subMenu: {
              sub: [
                {
                  name: " Cell phone S",
                },
              ],
            },
          },
        ],
      },
    },

    {
      name: "Audio",
      subMenu: {
        active: false,
        sub: [
          {
            name: " Auxiliary Input Cables",
          },
          {
            name: " Cables & Chargers",
          },

          {
            name: "bb",
            subMenu: {
              sub: [
                {
                  name: "bb1",
                },
                {
                  name: "bb2",
                },
                {
                  name: "bbb",
                  subMenu: {
                    sub: [
                      {
                        name: "bbb1",
                      },
                      {
                        name: "bbb2",
                      },
                    ],
                  },
                },
              ],
            },
          },
        ],
      },
    },
  ]);

  const showList = (arr) => {
    return (
      <div>
        {arr.map((value, key) => {
          if (value.subMenu) {
            return (
              <div key={key}>
                <DynamicListItem
                  showList={(arr) => {
                    return showList(arr);
                  }}
                  value={value}
                />
              </div>
            );
          } else {
            return (
              <li key={key}>
                {/* <FontAwesomeIcon icon={faAngleRight} /> */}
                {value.name}
              </li>
            );
          }
        })}
      </div>
    );
  };

  return (
    <div className="content-wrapper">
      <aside>
        <section>
          <div className="menu">
            <ul>{showList(menu)}</ul>
          </div>
        </section>
        <Brands handleOnchange={(e) => onChange(e)} setSearch1={setSearch} />
        <div className="">
          <select value={sort} onChange={(e) => setSort(e.target.value)}>
            <option value=""></option>
            <option value="price">Tăng dần</option>
            <option value="price&_order=desc">Giảm dần</option>
          </select>
        </div>
        <div className="Ratings-title">
          <h3>Ratings</h3>
        </div>
        <Ratings value={rating} onChange={(value) => handeChangeRating(value)} />
        Up to 5<br />
        <div className="Prices">
          <div className="Prices-title">
            <h3>Prices</h3>
          </div>
          <div className="Prices-range">
            <button
              value="price_lte=1"
              onClick={(e) => {
                setPrice(e.target.value);
              }}
            >
              ≤ 1
            </button>
            <button
              value="price_lte=80&price_gte=1"
              onClick={(e) => {
                setPrice(e.target.value);
              }}
            >
              {" "}
              $1-80
            </button>
            <button
              value="price_lte=160&price_gte=80"
              onClick={(e) => {
                setPrice(e.target.value);
              }}
            >
              {" "}
              $80-160
            </button>
            <button
              value="price_lte=240&price_gte=160"
              onClick={(e) => {
                setPrice(e.target.value);
              }}
            >
              {" "}
              $160-240
            </button>
            <button value="price_gte=280">≥ $280</button>
          </div>

          <form>
            <label>
              <span />$
              <input
                type="number"
                min="1"
                value={min}
                onChange={(e) => {
                  setMin(e.target.value);
                }}
              />
            </label>
            <span />
            to
            <span />$
            <label>
              <input
                type="number"
                value={max}
                onChange={(e) => {
                  setMax(e.target.value);
                }}
              />
            </label>
            <button type="submit">Go</button>
          </form>
        </div>
      </aside>
      <div className="page">
        <button
          value="1"
          onClick={(e) => {
            setPage(e.target.value);
          }}
        >
          1
        </button>
        <button
          value="2"
          onClick={(e) => {
            setPage(e.target.value);
          }}
        >
          2
        </button>
        <button
          value="3"
          onClick={(e) => {
            setPage(e.target.value);
          }}
        >
          3
        </button>
      </div>
      <Product data={data} id={id} />
    </div>
  );
};

export default Home;

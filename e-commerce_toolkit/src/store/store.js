import { configureStore } from "@reduxjs/toolkit";
import handleProduct from "../features/handleProduct";
import handleQuality from "../features/handleQuality";

export const store = configureStore({
  reducer: {
    handleQuality: handleQuality,
    handleProduct: handleProduct,
  },
});
